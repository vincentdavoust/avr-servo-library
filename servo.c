/*
** servo.c for servo in /home/vincent/skynavion/soft/libSERVO
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Mon Jan  4 16:13:51 2016 Vincent Davoust
** Last update Sun Jan 10 18:41:26 2016 Vincent Davoust
**
** Prefix : SERVO
*/

#include "servo.h"

void SERVO_cmd(int servo, int value) {
  if (servo > 7		|| servo < 0	|| \
      value > 1000	|| value < 0)
    return;

  SERVO_pos[servo] = value + 1000;
}

void SERVO_clock() {
  SETBIT(SERVO_PORT, SERVO_CLK);	// clock high
  _delay_us(20);			// small clock delay
  CLRBIT(SERVO_PORT, SERVO_CLK);	// clock low
}

void SERVO_reset() {
  CLRBIT(SERVO_PORT, SERVO_CLR);	// !reset low
  CLRBIT(SERVO_PORT, SERVO_IN);		// input low
  SERVO_clock();
}

void SERVO_set() {
  SERVO_reset();
  SETBIT(SERVO_PORT, SERVO_CLR);	// !reset high
  SETBIT(SERVO_PORT, SERVO_IN);		// input high

}

void SERVO_shift() {
  SERVO_index++;
  if (SERVO_index > 7) {
    SERVO_reset();
    TMR0_int(SERVO_start, 5000); // value to be set to 20 000 - sum of servo_pos
    return ;
  }
  SERVO_clock();
  CLRBIT(SERVO_PORT, SERVO_IN);		// input low
  TMR0_int(SERVO_shift, SERVO_pos[SERVO_index]);
}

void SERVO_start() {
  SERVO_index = 0;
  SERVO_set();
  TMR0_int(SERVO_shift, SERVO_pos[0]);
}

void SERVO_stop() {
  TMR0_attachInt(TMR_nop);
  TMR0_stop();
}

void SERVO_init() {
  int i;
  for (i = 0; i < 8; i++) {
    SERVO_cmd(i, 1500);
  }
  SERVO_index = 0;


}
