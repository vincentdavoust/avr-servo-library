/*
** servo.h for servo in /home/vincent/skynavion/soft/libSERVO
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Mon Jan  4 16:14:05 2016 Vincent Davoust
** Last update Sun Jan 10 18:38:42 2016 Vincent Davoust
*/

#ifndef SERVO_H_
# define SERVO_H_


#ifdef SKYNAVION_V2
#include "../env/skynavion_v2.h"
#else
#include "../libraries.h"
#endif

// private
void SERVO_clock();
void SERVO_reset();
void SERVO_set();
void SERVO_shift();

// public
void SERVO_cmd(int servo, int value);
void SERVO_start();
void SERVO_stop();
void SERVO_init();

int SERVO_pos[8];
int SERVO_index;

#endif /* !SERVO_H_ */
